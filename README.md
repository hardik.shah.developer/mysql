MySql Table Size
------------------------------------------------------------------------------------------
```
SELECT 
	table_name, 
	(data_length+index_length)/power(1024,2) tablesize_mb 
FROM information_schema.tables 
WHERE table_schema='<yourtablename>' 
ORDER BY tablesize_mb desc;
```

Adding only one mode to sql_mode without removing existing ones:
------------------------------------------------------------------------------------------
```
SET sql_mode=(SELECT CONCAT(@@sql_mode,',<mode_to_add>'));
```

Removing only a specific mode from sql_mode without removing others:
------------------------------------------------------------------------------------------
```
SET sql_mode=(SELECT REPLACE(@@sql_mode,'<mode_to_remove>',''));
```

Remove only ONLY_FULL_GROUP_BY mode, then use below command:
------------------------------------------------------------------------------------------
```
SET sql_mode=(SELECT REPLACE(@@sql_mode, 'ONLY_FULL_GROUP_BY', ''));
```

Load data from CSV file to table
------------------------------------------------------------------------------------------
```
LOAD DATA LOCAL INFILE <csv_file_path> INTO TABLE <table_name> FIELDS TERMINATED BY ',' ENCLOSED BY '"' LINES TERMINATED BY '\n' IGNORE 1 ROWS;
```

Table ROW Format
------------------------------------------------------------------------------------------
```
SELECT 
	row_format 
FROM information_schema.tables 
WHERE table_schema="YOUR DB" 
AND table_name="YOUR TABLE" 
LIMIT 1;
```

Number of rows in each table of DB
------------------------------------------------------------------------------------------
```
SELECT 
	table_name, table_rows
FROM INFORMATION_SCHEMA.TABLES
WHERE TABLE_SCHEMA = 'yourDatabaseName';
```

Find rows with Special chars
------------------------------------------------------------------------------------------
```
SELECT 
	<fields> 
FROM <table>
WHERE LENGTH(<field>) <> CHAR_LENGTH(<field>);
```

Find and replace string in table
------------------------------------------------------------------------------------------
```
UPDATE tbl_name
SET
    field_name = REPLACE(field_name,
        string_to_find,
        string_to_replace)
WHERE
    conditions;
```